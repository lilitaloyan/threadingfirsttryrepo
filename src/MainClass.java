import sun.tools.jar.Main;

/**
 * Created by Lilok on 2/14/2018.
 */
public class MainClass {


    public static void main(String args[]) {

        final Object lock = new Object();

        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i < 100; i++) {
                    System.out.println(i);
                    if (i % 10 == 0) {
                        synchronized (lock) {


                            try {
                                lock.notify();
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
        th.start();


        //☺☺☺o☺ñ♦44♣♠•◘○♦♣♠☺☻☻ alt 1,2,3 ect.
        Thread thOne = new Thread(() -> {

            for (int i = 1; i < 100; i++) {
                System.out.println("☺" + i);
                if (i % 10 == 0) {
                    synchronized (lock) {
                        try {
                            lock.notify();
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        thOne.start();
    }
}
